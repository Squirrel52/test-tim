$(document).ready(function(){
    $('.head-slider').slick({
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>',
    });

    $('.hover-button').mouseover(function(){
        var parent = event.target.closest('.block-subscription');
        $(parent).find('.head-content').css('background', '#49cbcd');
    });
    $('.hover-button').mouseout(function(){
        var parent = event.target.closest('.block-subscription');
        $(parent).find('.head-content').removeAttr('style');
    });

    $('#menu-toggle').click(function() {
        $('#menu-list').slideToggle( "slow" );
    });
});