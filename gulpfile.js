var gulp = require('gulp');
var concat = require('gulp-concat');
var cssMin = require('gulp-css');

gulp.task('default', function () {
    return gulp.src('./css/**/*.css')
        .pipe(cssMin())
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./css/'));
});